# In Node.js


## Summary
Leetcode problems' solutions in Javascript.

## Description
This is a basic repo storing my solutions to the problems list found at https://leetcode.com/problemset/all to continue my personal growth as a software developer. Since this is a basic repo, this readme is going to be simple as well. Each code file is dedicated to creating a solution to a particular problem. Since there are over 2000 problems at the time of this repo's creation, each file name will be using leading zeros where needed. There are going to be 2 basic file types, code files and an accompanying text file that contains supplementary information such as an explanation, hints to finding the solution and/or gotchas.

## Installation
WIP. Plans currently are to create a website where this code can be run in a browser. However, I am new to JS and will be starting out coding to use Node.js. Since it is a library, I am confident that a solution can be found or made.

## Usage
For now, run each file in Node.js.

## Support
Issues should be put in the Issue tracker in this repo.

## Roadmap
[] Convert my C# based solutions to JS
[] Create a website for showing these solutions
[] Continue solving the first 80 problems as those are typical to technical interviews
[] Continue solving the problems at a to be determined pace (weekly seems likely).

## Contributing
Since this is a personal portfolio project, contributions should be limited to posting issues related to running the solutions.

## License
All software contained within is licensed under the Apache v2 license. See the LICENSE file for the full text.

## Project status
Due to the nature of this project, development will happen in fits and spurts.
